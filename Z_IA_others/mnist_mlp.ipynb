{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Digit classification using neural networks\n",
    "\n",
    "## First, we import the needed libraries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pickle\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib as mpl\n",
    "\n",
    "mpl.style.use('grayscale') #to print the images in the grayscale format\n",
    "np.random.seed(42) #set the random seed for experiment replication\n",
    "num_classes = 10 #specify the number of classes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Let's start loading the data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(\"mnist.pkl\", 'rb') as f:\n",
    "    mnist = pickle.load(f, encoding='latin1')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## mnist will be a dictionary containing the training and test data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_set, valid_set, test_set = mnist\n",
    "train_images, train_labels = train_set\n",
    "valid_images, valid_labels = valid_set\n",
    "test_images, test_labels = test_set"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## You should always check the dimension of your data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(train_images.shape, train_labels.shape, \n",
    "      valid_images.shape, valid_labels.shape,\n",
    "      test_images.shape, test_labels.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Let's display images from the training set"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image = train_images[30].reshape(28, 28)\n",
    "plt.imshow(image)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The labels need to be in one-hot vector form, e.g label 4 becomes (0, 0, 0, 0, 1, 0, ..., 0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def transform_to_onehot(labels):\n",
    "    one_hot_labels = np.zeros((labels.shape[0], num_classes))\n",
    "    one_hot_labels[np.arange(labels.shape[0]), labels] = 1\n",
    "    return one_hot_labels"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Let's convert the labels"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Before one hot encoding: ', train_labels[0])\n",
    "y = transform_to_onehot(train_labels)\n",
    "print('After one hot encoding: ', y[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Now, the real learning begins\n",
    "\n",
    "### We start by defining the hyperparameters of the network"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "epochs = 200\n",
    "learning_rate = 0.5\n",
    "hidden_layer_size = 16"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### We initialize the parameters to be learned, W (weights) and b (biases)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "W1 = np.random.normal(0, 0.001, (hidden_layer_size, train_images.shape[1]))\n",
    "b1 = np.zeros(hidden_layer_size)\n",
    "W2 = np.random.normal(0, 0.001, (num_classes, hidden_layer_size))\n",
    "b2 = np.zeros(num_classes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### We need to compute the value of the output:\n",
    "#### $z_1 = x W_1^T + b_1$\n",
    "#### $a_1 = relu(z_1)$\n",
    "#### $z_2 = a_1 W_2^T + b_2$\n",
    "#### $a_2 = softmax(z_2)$\n",
    "### We define the relu function: $max(0, x)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def relu(x):\n",
    "    return np.maximum(0, x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### We define the softmax function: $\\frac{e^{x_i}}{\\sum_j e^{x_j}}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def softmax(x):\n",
    "    exp = np.exp(x)\n",
    "    return exp / np.sum(exp, axis=1, keepdims=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### We can now implement the forward function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def forward(x):\n",
    "    z1 = np.matmul(x, W1.T) + b1\n",
    "    a1 = relu(z1)\n",
    "    z2 = np.matmul(a1, W2.T) + b2\n",
    "    a2 = softmax(z2)\n",
    "    \n",
    "    cache = (x, a1, a2)\n",
    "    return a2, cache"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### As a loss function we will be using cross entropy:\n",
    "## $H = -\\frac{1}{n} \\sum_i y_i \\log(p_i)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def cross_entropy_loss(y_pred, y_true):\n",
    "    logprobs = y_true * np.log(y_pred)\n",
    "    cost = - (1.0 / y_true.shape[0]) * np.sum(logprobs)\n",
    "    return cost"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### We also need to compute the gradients with respect to W and b, to update them:\n",
    "### $\\frac{\\partial H}{\\partial z^{(2)}} = \\delta^{(2)} = \\frac{1}{n} (a^{(2)} - \\hat y) $\n",
    "### $\\frac{\\partial H}{\\partial W^{(2)}} =  (\\delta^{(2)})^T a_1$\n",
    "### $\\frac{\\partial H}{\\partial b^{(2)}} = \\sum^n_{i = 1} \\delta^{(2)}_i$\n",
    "\n",
    "### $\\frac{\\partial H}{\\partial z^{(1)}} = \\delta^{(1)} = \\delta^{(2)}  W^{(2)}  relu(z^{(1)})' $\n",
    "\n",
    "### $\\frac{\\partial H}{\\partial W^{(1)}} = (\\delta^{(1)})^T x$\n",
    "### $\\frac{\\partial H}{\\partial b^{(1)}} = \\sum^n_{i = 1} \\delta^{(1)}_i$\n",
    "\n",
    "### As you can see, we need the derivative of the relu function, which is 1 if the function is positive and 0 if the function is less or equal than 0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def relu_backward(x):\n",
    "    x[x <= 0.0] = 0.0\n",
    "    x[x > 0.0] = 1.0\n",
    "    return x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_grads(cache, y):\n",
    "    x, a1, a2 = cache\n",
    "    \n",
    "    delta2 = (1.0 / train_images.shape[0]) * (a2 - y)\n",
    "    dw2 = np.matmul(delta2.T, a1)\n",
    "    db2 = np.sum(delta2, axis=0, keepdims=True)\n",
    "    \n",
    "    delta1 = np.matmul(delta2, W2) * relu_backward(a1)\n",
    "\n",
    "    dw1 = np.matmul(delta1.T, x)\n",
    "    db1 = np.sum(delta1, axis=0, keepdims=True)\n",
    "    \n",
    "    return dw2, db2, dw1, db1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## We can now begin the optimization loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for epoch in range(epochs):\n",
    "\n",
    "    out, cache = forward(train_images)\n",
    "\n",
    "    loss = cross_entropy_loss(out, y)\n",
    "\n",
    "    grads = compute_grads(cache, y)\n",
    "\n",
    "    W2 = W2 - learning_rate * grads[0]\n",
    "    b2 = b2 - learning_rate * grads[1]\n",
    "    W1 = W1 - learning_rate * grads[2]\n",
    "    b1 = b1 - learning_rate * grads[3]\n",
    "\n",
    "    print(\"Loss at epoch {}: {}\".format(epoch + 1, loss))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## That's all! As you can see the training has converged. Let's now see the accuracy on the test set. We start by defining an accuracy function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def accuracy(y_pred, y_true):\n",
    "    y_pred = np.argmax(y_pred, axis=1)\n",
    "    accuracy = ((y_pred == y_true).sum() * 100) / y_true.shape[0]\n",
    "    return accuracy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Having the weights learned, we now simply propagate the input through the network:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pred, _ = forward(test_images)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Let's call the accuracy method to see how good our network is doing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_accuracy = accuracy(pred, test_labels)\n",
    "print(\"Test accuracy: {}%\".format(test_accuracy))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## It seems that the network is doing its job very good! Unfortunatelly, MNIST is a toy dataset and an accuracy of ~92% is not that impressive. Nevertheless, let's see some real predictions for different digits:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "digit_to_test = 7\n",
    "indexes_digit = test_images[test_labels == digit_to_test] #return the indexes where digit appears\n",
    "\n",
    "test_image = indexes_digit[10]\n",
    "resized_image = test_image.reshape(28, 28)\n",
    "plt.imshow(resized_image)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## This is how the digit looks after plotting it. And now, for the prediction:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "prediction, _ = forward(test_image) #compute the forward propagation\n",
    "\n",
    "max_value_digit = np.argmax(prediction)\n",
    "print(\"The digit predicted is: %d\" % max_value_digit)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.7.6 64-bit ('ia': conda)",
   "language": "python",
   "name": "python37664bitiaconda90a31a080f914cbaa28bfd972b8621f4"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7-final"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}