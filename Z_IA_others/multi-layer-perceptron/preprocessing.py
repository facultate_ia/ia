import pandas as pd
import numpy as np

def load_dataset(file_path:str, separator:chr) -> np.ndarray:
    """
    Reads the dataset.
    
    Parameters
    ----------
    
    file_path (str): the path to the input training file
    separator (chr): the character that separates values in the file
    
    returns: the dataset as a numpy array
    """
    return pd.read_csv(file_path, separator, header = None).values

def filter_set(input_matrix:np.ndarray, first_digit:int, second_digit:int) -> np.ndarray:
    """
    Filters the input matrix based on the desired 2 values and returns a matrix that has on its first column values in 
    the tuple (first_digit, second_digit).

    Parameters
    ----------

    initial_matrix (numpy.ndarray): the dataset
    first_digit(int): the first value
    second_digit(int): the second value

    returns: the filtered matrix
    """
    return input_matrix[(input_matrix[:, 0] == first_digit) | (input_matrix[:, 0] == second_digit)]

def extract_x_and_y(initial_matrix:np.ndarray) -> tuple:
    """
    Extracts X and Y matrices.
    
    Parameters
    ----------
    initial_matrix (numpy.ndarray): the dataset
    
    returns: a tuple containing X and Y
    """
    return initial_matrix[:, 1:], initial_matrix[:, 0][:, np.newaxis]

def insert_ones_column(matrix:np.ndarray) -> np.ndarray:
    """
    Inserts a column of 1 values.
    """
    return np.insert(matrix, 0, 1, 1)

def normalize2(x:np.ndarray):
    """
    Just divide by 255.
    """
    return x / 255

def normalize(input_matrix:np.ndarray) -> np.ndarray:
    """
    Normalizes a given input matrix with values in [0, 1].
    
    Parameters
    ----------
    input_matrix (numpy.ndarray): the matrix to be normalized
    
    returns: the normalized matrix
    """
    min_values = input_matrix.min(axis = 0)
    max_values = input_matrix.max(axis = 0)
    non_constant = min_values != max_values

    input_matrix[:, non_constant] = (input_matrix[:, non_constant] - min_values[non_constant]) / (max_values[non_constant] - min_values[non_constant])

    return input_matrix
    